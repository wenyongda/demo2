<%@ page import="com.student.pojo.Book" %><%--
  Created by IntelliJ IDEA.
  User: 12241
  Date: 2020/10/23
  Time: 13:39
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
String path=request.getContextPath();
String basePath=request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<html>
<head>
    <base href=""<%=basePath%>">
    <title>增加图书</title>
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="Cache-Control" content="no-cache">
    <meta http-equiv="Expires" content="0">
    <meta http-equiv="Keywords" content="keyword1,keyword2,keyword3">
    <meta http-equiv="description" content="This is my page">
    <%
        Book book=(Book)request.getAttribute("book");
    %>
</head>
<body>
    <center>
        <h3>修改图书</h3>
        <form action="UpBookServlet" method="post">
            <input type="hidden" name="id" value="<%=book.getId()%>">
                名称：<input type="text" name="name" value="<%=book.getName()%>">
                类型：<input type="text" name="stype" value="<%=book.getStype()%>">
                出版社：<input type="text" name="publisher" value="<%=book.getPublisher()%>">
                作者：<input type="text" name="author" value="<%=book.getAuthor()%>">
                价格：<input type="text" name="price" value="<%=book.getPrice()%>">
            <input type="submit" value="保存">
        </form>
    </center>
</body>
</html>
